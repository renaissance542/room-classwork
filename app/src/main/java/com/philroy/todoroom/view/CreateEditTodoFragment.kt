package com.philroy.todoroom.view

import android.os.Build
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.philroy.todoroom.databinding.FragmentCreateEditTodoBinding
import com.philroy.todoroom.model.RepoImpl
import com.philroy.todoroom.model.models.Todo
import com.philroy.todoroom.viewmodels.CreateEditTodoViewModel
import com.philroy.todoroom.viewmodels.factories.ViewModelFactoryCreateEdit
import java.time.Clock
import java.time.Instant

class CreateEditTodoFragment: Fragment() {
    private var _binding: FragmentCreateEditTodoBinding? = null
    private val binding: FragmentCreateEditTodoBinding get() = _binding!!

    private val repoImpl by lazy {
        RepoImpl(requireContext())
    }

    private val viewModel by viewModels<CreateEditTodoViewModel> {
        ViewModelFactoryCreateEdit(repoImpl)
    }

    private val args by navArgs<CreateEditTodoFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCreateEditTodoBinding.inflate(inflater, container, false)
        .also { _binding = it }
        .root

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.title = args.todo.title
        viewModel.description = args.todo.description
        initViews()
        initListeners()
    }

    private fun initViews() = with(binding) {
        todoTitle.text = SpannableStringBuilder(args.todo.title)
        todoDescription.text = SpannableStringBuilder(args.todo.description)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun initListeners() = with(binding){
        validateInputs()
        viewModel.continueButton.observe(viewLifecycleOwner) { onOrOff ->
               createTodoBtn.isEnabled = onOrOff
        }
        todoTitle.addTextChangedListener {
            viewModel.title = it.toString()
            validateInputs()
        }

        todoDescription.addTextChangedListener {
            viewModel.description = it.toString()
            validateInputs()
        }

        createTodoBtn.setOnClickListener {
            val clock: Clock = Clock.systemDefaultZone()
            val time = clock.instant().toString()
            val instantBefore = Instant.parse(time)
            val todo = args.todo
            todo.title = viewModel.title
            todo.description = viewModel.description
            todo.completed = true
            todo.updated = instantBefore.toString()
            todo.date = instantBefore.toString()
            viewModel.addTodoItem(todo)
            findNavController().navigateUp()
        }

        deleteTodoBtn.setOnClickListener {
            viewModel.deleteTodoItem(args.todo)
            findNavController().navigateUp()
        }

    }

    private fun validateInputs() {
        if (viewModel.title.isNotEmpty() && viewModel.description.isNotEmpty()){
            viewModel.setContinueButtonValue(true)
        } else {
            viewModel.setContinueButtonValue(false)
        }
    }
}














