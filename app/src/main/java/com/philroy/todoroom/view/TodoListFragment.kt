package com.philroy.todoroom.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.philroy.todoroom.databinding.FragmentTodoListBinding
import com.philroy.todoroom.model.RepoImpl
import com.philroy.todoroom.model.models.Todo
import com.philroy.todoroom.util.Resource
import com.philroy.todoroom.view.adapters.TodoAdapter
import com.philroy.todoroom.viewmodels.TodoListViewModel
import com.philroy.todoroom.viewmodels.factories.ViewModelFactoryTodoList

class TodoListFragment: Fragment() {
    private var _binding: FragmentTodoListBinding? = null
    private val binding: FragmentTodoListBinding get() = _binding!!

    val repoImpl by lazy {
        RepoImpl(requireContext())
    }

    val viewModel by viewModels<TodoListViewModel> {
        ViewModelFactoryTodoList(repoImpl)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentTodoListBinding.inflate(inflater, container, false)
        .also{ _binding = it }
        .root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    override fun onResume() {
        super.onResume()
        viewModel.grabAllTodos()
    }
    private fun initListeners() = with(binding){
        recyclerViewTodo.layoutManager = LinearLayoutManager(context)
        viewModel.todoItems.observe(viewLifecycleOwner) { viewState ->
            when (viewState){
                is Resource.Error -> {
                    Toast.makeText(context, viewState.errorMsg, Toast.LENGTH_SHORT)
                }
                is Resource.Loading -> {
                    // Circular progress bar
                }
                is Resource.Success -> {
                    // Hide progress bar
                    this.recyclerViewTodo.adapter = TodoAdapter(::navigateToCreateEditTodo).apply {
                        applyItems(viewState.data)
                    }
                }
            }
        }

        addTodoBtn.setOnClickListener {
            val action = TodoListFragmentDirections.actionTodoListFragmentToCreateEditTodoFragment(Todo())
            findNavController().navigate(action)
        }
    }

    private fun navigateToCreateEditTodo(todo: Todo = Todo()) {
        val action = TodoListFragmentDirections.actionTodoListFragmentToCreateEditTodoFragment(todo)
        findNavController().navigate(action)
    }

}
