package com.philroy.todoroom.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.philroy.todoroom.databinding.ViewTodoItemBinding
import com.philroy.todoroom.model.models.Todo

class TodoAdapter(
    val navigateToCreateEdit: (todo: Todo) -> Unit
): RecyclerView.Adapter<TodoAdapter.TodoViewHolder>(){

    lateinit var data: List<Todo>

    class TodoViewHolder(
        val binding: ViewTodoItemBinding
    ): RecyclerView.ViewHolder(binding.root){
        fun applyItem(todo: Todo) {
            binding.todoNum.text = (adapterPosition + 1).toString()
            binding.todoItem.text = todo.title
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        val binding = ViewTodoItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TodoViewHolder(binding).apply {
            binding.root.setOnClickListener {
                navigateToCreateEdit(data[adapterPosition])
            }
        }
    }

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        val todoItem = data[position]
        holder.applyItem(todoItem)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun applyItems(todos: List<Todo>){
        data = todos
    }
}