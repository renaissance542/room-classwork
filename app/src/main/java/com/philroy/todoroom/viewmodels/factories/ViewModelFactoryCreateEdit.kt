package com.philroy.todoroom.viewmodels.factories

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.philroy.todoroom.model.RepoImpl
import com.philroy.todoroom.viewmodels.CreateEditTodoViewModel

class ViewModelFactoryCreateEdit(
    private val repo: RepoImpl
):ViewModelProvider.NewInstanceFactory() {

    override fun <T: ViewModel> create(modelClass: Class<T>): T {
        return CreateEditTodoViewModel (repo) as T
    }
}