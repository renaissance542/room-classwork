package com.philroy.todoroom.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.philroy.todoroom.model.RepoImpl
import com.philroy.todoroom.model.models.Todo
import com.philroy.todoroom.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.DisposableHandle
import kotlinx.coroutines.launch

class TodoListViewModel(val repo: RepoImpl): ViewModel() {

    private var _todoItems: MutableLiveData<Resource<List<Todo>>> = MutableLiveData(Resource.Loading<List<Todo>>())
    val todoItems: LiveData<Resource<List<Todo>>> get() = _todoItems


    fun grabAllTodos() = viewModelScope.launch(Dispatchers.Main) {
            val response = repo.grabAllTodos()
            _todoItems.value = response
    }

}