package com.philroy.todoroom.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.philroy.todoroom.databinding.FragmentCreateEditTodoBinding
import com.philroy.todoroom.model.RepoImpl
import com.philroy.todoroom.model.models.Todo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CreateEditTodoViewModel(val repo: RepoImpl): ViewModel() {
    var title: String = ""
    var description: String = ""

    private var _continueButton = MutableLiveData<Boolean>(false)
    val continueButton: LiveData<Boolean> get() = _continueButton

    fun setContinueButtonValue(flag: Boolean) {
        _continueButton.value = flag
    }

    fun addTodoItem(todo: Todo) = viewModelScope.launch(Dispatchers.Main) {
        repo.addTodo(todo)
    }

    fun deleteTodoItem(todo: Todo) = viewModelScope.launch(Dispatchers.Main) {
        repo.deleteTodo(todo)
    }

}