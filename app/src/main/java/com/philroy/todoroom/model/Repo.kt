package com.philroy.todoroom.model

import com.philroy.todoroom.model.models.Todo
import com.philroy.todoroom.util.Resource

interface Repo {
    suspend fun addTodo(todo: Todo)
    suspend fun editTodo(todo: Todo)
    suspend fun deleteTodo(todo: Todo)
    suspend fun grabAllTodos(): Resource<List<Todo>>
}