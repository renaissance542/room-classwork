package com.philroy.todoroom.model.room

import androidx.room.*
import com.philroy.todoroom.model.models.Todo

@Dao
interface TodoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addTodoItem(todo: Todo)

    @Query("SELECT * FROM todo_item")
    suspend fun grabAllTodos(): List<Todo>

    @Delete
    suspend fun deleteTodo(todo: Todo)
}