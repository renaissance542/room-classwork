package com.philroy.todoroom.model

import android.content.Context
import com.philroy.todoroom.model.models.Todo
import com.philroy.todoroom.model.room.TodoDatabase
import com.philroy.todoroom.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

class RepoImpl(context: Context): Repo {

    private val todoDao = TodoDatabase.getDatabaseInstance(context).todoDao()

    override suspend fun addTodo(todo: Todo) = withContext(Dispatchers.IO){
        todoDao.addTodoItem(todo)
    }

    override suspend fun editTodo(todo: Todo)  = withContext(Dispatchers.IO){
        todoDao.addTodoItem(todo)
    }

    override suspend fun deleteTodo(todo: Todo) = withContext(Dispatchers.IO){
        todoDao.deleteTodo(todo)
    }

    override suspend fun grabAllTodos(): Resource<List<Todo>>  = withContext(Dispatchers.IO){
        return@withContext try {
            val response = todoDao.grabAllTodos()
            Resource.Success(response)
        } catch (e: Exception) {
            Resource.Error(e.localizedMessage ?: "Default error message || DB connection failed")
        }
    }
}