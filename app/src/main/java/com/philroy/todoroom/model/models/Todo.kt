package com.philroy.todoroom.model.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "todo_item")
@Parcelize
data class Todo(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var title: String = "",
    var description: String = "",
    var completed: Boolean = false,
    var date: String = "",
    var updated: String = ""
): Parcelable
